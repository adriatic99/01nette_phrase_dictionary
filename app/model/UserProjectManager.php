<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Model;

use Nette;

class UserProjectManager
{
    use Nette\SmartObject;

    /**
     * @var Nette\Database\Context
     */
    private $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function getAllUsers()
    {
        return $this->database->table('userprojects');
    }
    
    public function getUserByUsername($username)
    {
        return $this->database->table('userprojects')
               ->where('username = $username');
    }
    
    public function saveNewUser($username, $password, $email)
    {
        $this->database->table('userprojects')
            ->insert($username, $password, $email);
    }
}

